import React, { useState } from "react";
import "./App.css";

function App() {
  const [state, setState] = useState({
    num: "",
    calType: "PRIME",
    isMatch: false,
  });

  const isPrime = (num) => {
    const lstVal = Math.floor(Math.sqrt(num));
    if (num < 2 || isNaN(num)) return false;
    for (let i = 2; i <= lstVal; i++) {
      if (num % i === 0) return false;
    }
    return true;
  };
  const isFibo = (num) => {
    if (isNaN(num)) return false;
    if (num <= 3) return true;
    let aN1 = 2,
      aN = 3;
    while (aN < num) {
      aN = aN + aN1;
      aN1 = aN - aN1;
    }
    if (aN === num) return true;
    return false;
  };

  const calculateNum = (num, calType) => {
    let isMatch = false;
    switch (calType) {
      case "PRIME":
        isMatch = isPrime(parseInt(num));
        break;
      case "FIBO":
        isMatch = isFibo(parseInt(num));
        break;
      default:
    }
    setState({
      num,
      calType,
      isMatch,
    });
  };
  const onNumberChange = (e) => {
    let value = e.target.value;
    if (+value < 0) value = "1";
    else if (+value % 1 !== 0) value = Math.round(value).toString();
    calculateNum(value, state.calType);
  };

  const onSelectChange = (e) => {
    const value = e.target.value;
    calculateNum(state.num, value);
  };

  const { num, calType, isMatch } = state;
  return (
    <div className="App">
      <div className="col1">
        <input value={num} onChange={onNumberChange} type="number" />
      </div>
      <div className="col2">
        <select
          value={calType}
          onChange={onSelectChange}
          className="select-cal-type"
        >
          <option value="PRIME">isPrime</option>
          <option value="FIBO">isFibonacci</option>
        </select>
      </div>
      <div className="col3">{isMatch ? "true" : "false"}</div>
    </div>
  );
}

export default App;
